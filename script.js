let tabs = document.querySelector(".tabs");
let tabsTitle = tabs.children;
tabsTitle=[...tabsTitle];
let tabsContent = document.querySelector(".tabs-content");
let text = tabsContent.children;
text=[...text];
for (let item of text)
{
    item.hidden = true;
}
tabs.addEventListener("click",event => {

    let index = tabsTitle.indexOf(event.target);

    for (let i = 0;i<tabsTitle.length;i++){
        if(i===index)
        {
            tabsTitle[i].classList.add("active");
            text[i].hidden=false;
        }
         else
        {
        text[i].hidden = true;
        tabsTitle[i].classList.remove("active");
        }
    }
});